# This file is a template, and might need editing before it works on your project.
FROM golang:alpine AS builder

RUN apk --no-cache add ca-certificates git

RUN go get github.com/gordonklaus/ineffassign

WORKDIR /go/src/github.com/gordonklaus/ineffassign

RUN CGO_ENABLED=0 GOOS=linux go build -v -a -installsuffix cgo -o ineffassign .

FROM alpine:latest

WORKDIR /usr/bin/

COPY --from=builder /go/src/github.com/gordonklaus/ineffassign/ineffassign .
CMD ["./ineffassign"]
